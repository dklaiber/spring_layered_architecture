package com.qualogy.spring_layered_architecture.service.impl;


import com.qualogy.spring_layered_architecture.dto.PersonDto;
import com.qualogy.spring_layered_architecture.model.Person;
import com.qualogy.spring_layered_architecture.repository.PersonRepository;
import com.qualogy.spring_layered_architecture.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<PersonDto> getAllPersons() {
        final List<Person> personList = personRepository.findAll();
        final List<PersonDto> personDtoList = new ArrayList<>();
        for (Person person : personList) {
            PersonDto personDto = new PersonDto();
            personDto.setFirstname(person.getFirstname());
            personDto.setLastname(person.getLastname());
            personDto.setEmail(person.getEmail());
            personDtoList.add(personDto);
        }
        return personDtoList;
    }

}
