package com.qualogy.spring_layered_architecture.service;

import com.qualogy.spring_layered_architecture.dto.PersonDto;

import java.util.List;

public interface PersonService {

    List<PersonDto> getAllPersons();

}
