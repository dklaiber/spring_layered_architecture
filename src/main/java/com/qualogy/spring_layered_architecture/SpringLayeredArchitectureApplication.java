package com.qualogy.spring_layered_architecture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLayeredArchitectureApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLayeredArchitectureApplication.class, args);
    }

}
