package com.qualogy.spring_layered_architecture.repository;


import com.qualogy.spring_layered_architecture.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
}
