IF NOT EXISTS(SELECT firstname
              FROM person
              WHERE firstname = 'John' AND lastname = 'Doe')
    INSERT INTO person (id, firstname, lastname, email, password)
    VALUES (1, 'John', 'Doe', 'johndoe@email.com', 'password');

IF NOT EXISTS(SELECT firstname
              FROM person
              WHERE firstname = 'Jane' AND lastname = 'Doe')
    INSERT INTO person (id, firstname, lastname, email, password)
    VALUES (2, 'Jane', 'Doe', 'janedoe@email.com', 'password');
