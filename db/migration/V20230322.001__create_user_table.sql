IF NOT EXISTS(SELECT *
              FROM sys.objects
              WHERE object_id = OBJECT_ID(N'[dbo].[person]')
                AND type in (N'U'))
CREATE TABLE person
(
    id    INT PRIMARY KEY,
    firstname VARCHAR(50),
    lastname  VARCHAR(50),
    email     VARCHAR(100),
    password  VARCHAR(50)
);

